package homework8.DAO;

import homework8.Family.Family;
import homework8.Family.Human;
import homework8.Family.Pet;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.*;

public class FamilyService {
    private FamilyDao familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyDao.getAllFamilies().forEach(el -> el.toString());
    }

    public Family getFamilyById(int infex) {
        return familyDao.getFamilyByIndex(infex);
    }

    public int count() {
        return familyDao.getAllFamilies().size();
    }

    public void createNewFamily(Human mother, Human father) {
        Family newFamily = new Family(mother, father);
        familyDao.saveFamily(newFamily);
    }

    public boolean deleteFamilyByIndex(int index) {
        boolean res = familyDao.deleteFamily(index);
        return res;

    }

    public Set<Pet> getPets(int index) {
        Family ret = familyDao.getFamilyByIndex(index);
        Set <Pet> pet = ret.getPet();
        return pet;
    }
    public void addPet(int index, Pet pet) {
        Family ret = familyDao.getFamilyByIndex(index);
        Set<Pet> peti = ret.getPet();
        peti.add(pet);
    }
    public List<Family> getFamiliesBiggerThen(int size) {
        List<Family> families = familyDao.getAllFamilies();
        List<Family> newFamilies = new ArrayList<>();
        families.forEach(el -> {
            if (el.countFamily(el.getChildren()) > size) {
                newFamilies.add(el);
            }

        });
        return newFamilies;
    }
    public int   countFamiliesWithMemberNumber (int size) {
        List<Family> families = familyDao.getAllFamilies();
        List<Family> newFamilies = new ArrayList<>();
        families.forEach(family -> {
            if (family.countFamily(family.getChildren()) == size) {
                newFamilies.add(family);
            }

        });
        return newFamilies.size();


    }

    public List<Family> getFamiliesLessThen(int size) {
        List<Family> families = familyDao.getAllFamilies();
        List<Family> newFamilies = new ArrayList<>();
        families.forEach(el -> {
            if (el.countFamily(el.getChildren()) < size) {
                newFamilies.add(el);
            }

        });
        return newFamilies;    }

    public Family  adoptChild(Family family,Human child){
        List<Family> families = familyDao.getAllFamilies();
        int index = families.indexOf(family);
        Family currentFamily = families.get(index);
        currentFamily.addChild(child);
        return currentFamily;
    }

    public Family bornChild(Family family,String girlName,String boyName){
        List<Family> families = familyDao.getAllFamilies();
        int index = families.indexOf(family);
        Random random =new Random();
        boolean isBoy = random.nextBoolean();
        String  name = (isBoy? boyName : girlName);
        Family currentFamily = families.get(index);
        Calendar calendar = new GregorianCalendar();
        long birthDay = calendar.getTimeInMillis();

            currentFamily.addChild(new Human(name,currentFamily.getFather().getSurname(),birthDay ));
        return currentFamily;
    }

    public void deleteAllChildrenOlderThen(int age){
        List<Family> families = familyDao.getAllFamilies();
        for(int i = 0;i < families.size();i++){
            List <Human> familyChildren = families.get(i).getChildren();
            Iterator<Human> it = familyChildren.iterator();
            for (; it.hasNext(); ) {
                Human child = it.next();
                LocalDate birthDay = Instant.ofEpochMilli(child.getBirthDate())
                        .atZone(ZoneId.systemDefault())
                        .toLocalDate();


                LocalDate now = LocalDate.now();
                Period period = Period.between(birthDay,now);

                if(period.getYears()  > age){
                    it.remove();}

            }
        }
    }

}

